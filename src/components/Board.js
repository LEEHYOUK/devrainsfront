import React, {PropTypes} from 'react';

import Toolbar from 'react-md/lib/Toolbars';

const Board = (props) => {
	return (
		<div className="height-all">
            <Toolbar className="md-divider-border md-divider-border--bottom" />
            <Toolbar className="md-divider-border md-divider-border--bottom" colored/>
			Board!!
			<p>boardName is {props.boardName}
				{props.articleNo == null
					? ""
					: " and articleNo is..." + props.articleNo}</p>
		</div>
	)
}

export default Board;
