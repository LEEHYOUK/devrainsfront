import React, {PropTypes} from 'react';
import TextField from 'react-md/lib/TextFields';
import Divider from 'react-md/lib/Dividers';
import Toolbar from 'react-md/lib/Toolbars'
import Button from 'react-md/lib/Buttons';

class Write extends React.Component {
	render() {
		return (
			<div>
				<Toolbar className="md-divider-border md-divider-border--bottom"/>
				<Toolbar className="md-divider-border md-divider-border--bottom" colored actions={<Button icon tooltipLabel = "보내기" onClick = {
					() => {
                        this.props.onSubmitArticle(this.articleContents.getField().value, this.articleTitle.getField().value, this.boardNo.getField().value, 1)
					}
				}>send</Button>}/>
				<TextField id="event-email" placeholder="Title" block paddedBlock ref={(ref) => {
					this.articleTitle = ref
				}}/>
				<Divider/>
				<TextField id="event-name" placeholder="????" block paddedBlock ref={(ref) => {
					this.boardNo = ref
				}}/>
				<Divider/>
				<TextField id="event-desc" placeholder="Description" block paddedBlock rows={4} ref={(ref) => {
					this.articleContents = ref
				}}/>
			</div>
		)
	}
}

export default Write;
