import React from 'react';
import {
    Router,
    Route,
    IndexRoute,
    browserHistory,
    Link,
    refresh
} from 'react-router';

import MainContainer from './containers/MainContainer';
import DashBoardContainer from './containers/DashBoardContainer';
import BoardContainer from './containers/BoardContainer';
import About from './components/About';
import Write from './components/Write';

class App extends React.Component {
    render() {
        return (
            <Router history={browserHistory}>
                <Route path="/" components={MainContainer}>
                    <IndexRoute components={DashBoardContainer}/>
                    <Route path="board" components={BoardContainer}>
                        <Route path=":boardName">
                            <Route path="new" components={Write} type={"write"}/>
                            <Route path=":articleNo" type={"read"}/>
                        </Route>
                    </Route>
                    <Route path="about" components={About}/>
                </Route>
            </Router>
        );
    }
}

export default App;
