import React, {PropTypes} from 'react'

import Board from '../components/Board';
import BoardLayout from '../layouts/BoardLayout';

import Write from '../components/Write';

import axios from 'axios';

import {serverUrl} from '../Config';

class BoardContainer extends React.Component {

	onSubmitArticle(articleContents, articleTitle, boardNo, userNo) {
		axios.post(serverUrl + "/user/article", {
			"articleTitle": articleTitle,
			"articleContents": articleContents,
			"boardNo": 1,
			"userNo": 1
		}).then((resp) => {
			console.log(resp);
		}).catch((err) => {
			console.log(err);
		});
	}

	render() {
		return (
			<BoardLayout boardName={this.props.params.boardName} articleNo={this.props.params.articleNo}>
				{this.props.children!=null?
					this.props.children
					:
					<Board boardName={this.props.params.boardName} articleNo={this.props.params.articleNo}/>
				}
			</BoardLayout>
		);
	}
}

export default BoardContainer;
