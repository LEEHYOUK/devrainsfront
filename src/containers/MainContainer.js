import React, { PropTypes } from 'react';
import axios from 'axios';

import {serverUrl} from '../Config';

import MainLayout from '../layouts/MainLayout'

class MainContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            boardList:[]
        };
    }

    componentDidMount() {
        axios.get(serverUrl+"/board").then((resp)=>{
            this.setState({boardList:resp.data});
        }).catch((err)=>{
            console.log(err);
        });
    }

    render () {
        return(
            <MainLayout children={this.props.children} location={this.props.location} router={this.props.router} boardList={this.state.boardList}/>
        );
    }
}

export default MainContainer;
