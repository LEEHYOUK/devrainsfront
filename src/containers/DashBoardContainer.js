import React, { PropTypes } from 'react';

import DashBoard from '../components/DashBoard';

class DashBoardContainer extends React.Component {
    render () {
        return(
            <DashBoard />
        );
    }
}

export default DashBoardContainer;
