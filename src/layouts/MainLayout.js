import React, {PropTypes} from 'react';

import {Link} from 'react-router';

import Drawer from 'react-md/lib/Drawers';
import Toolbar from 'react-md/lib/Toolbars';
import FontIcon from 'react-md/lib/FontIcons';

import {listActiveStyle} from '../util/commons';

const isActive = (to, path) => {
	return to === path;
}

const header = (
	<div>
		<Toolbar className="md-divider-border md-divider-border--bottom"/>
		<Toolbar className="md-divider-border md-divider-border--bottom" colored/>
	</div>
);
const MainLayout = (props) => {
	let boardListItems = [];
	for (let i = 0; i < props.boardList.length; i++) {
		boardListItems.push({
			component: Link,
			to: '/board/'+props.boardList[i].boardTitle,
			primaryText: props.boardList[i].boardTitle
		});
	}
	return (
		<div style={{
			backgroundColor: "#ffffff"
		}}>
			<Drawer defaultVisible type={Drawer.DrawerTypes.PERSISTENT} header={header} style={{
				zIndex: 100
			}} navItems={[
				{
					component: Link,
					to: '/',
					primaryText: 'Home',
					active: isActive('/', props.router.location.pathname),
					leftIcon: <FontIcon>home</FontIcon>
				}, {
					key: '/board',
					primaryText: 'Board',
					leftIcon: <FontIcon>view_list</FontIcon>,
					defaultVisible: true,
					active: props.router.isActive("/board"),
					nestedItems: boardListItems
				}
			]}/>
			<div className="md-drawer-relative height-all">
				{props.children}
			</div>
		</div>
	);
}

MainLayout.contextTypes = {
	router: React.PropTypes.object
}

export default MainLayout;
