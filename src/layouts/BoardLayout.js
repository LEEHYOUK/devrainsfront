import React, {PropTypes} from 'react';

import {List, ListItem} from 'react-md/lib/Lists';
import Toolbar from 'react-md/lib/Toolbars';
import TextField from 'react-md/lib/TextFields';
import Divider from 'react-md/lib/Dividers';
import {Link} from 'react-router';
import Paper from 'react-md/lib/Papers';
import Button from 'react-md/lib/Buttons';

import {listActiveStyle} from '../util/commons';

const BoardLayout = (props) => {
	return (
		<div className="md-grid md-grid--no-spacing height-all">
			<div className="md-cell md-cell--2 md-cell--phone-2">
				<Toolbar className="md-divider-border md-divider-border--bottom" title={props.boardName.toUpperCase()} titleStyle={{
					marginLeft: "16px"
				}}/>
				<Toolbar className="md-divider-border md-divider-border--bottom" colored>
					<TextField block paddedBlock placeholder="Search" className="md-cell--stretch"/>
				</Toolbar>
				<List>
					<ListItem primaryText="Inbox1" component={Link} to={"/board/" + props.boardName + "/1"} tileStyle={listActiveStyle(props.articleNo, 1)}/>
					<ListItem primaryText="Inbox2" component={Link} to={"/board/" + props.boardName + "/2"} tileStyle={listActiveStyle(props.articleNo, 2)}/>
					<ListItem primaryText="Inbox3" component={Link} to={"/board/" + props.boardName + "/3"} tileStyle={listActiveStyle(props.articleNo, 3)}/>
					<ListItem primaryText="Inbox4" component={Link} to={"/board/" + props.boardName + "/4"} tileStyle={listActiveStyle(props.articleNo, 4)}/>
				</List>
			</div>
			<Paper zDepth={1} className="md-cell md-cell--10 md-cell--phone-2 height-all">
				{props.children}
			</Paper>
		</div>
	);
}

export default BoardLayout;
