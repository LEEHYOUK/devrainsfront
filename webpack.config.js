var webpack = require('webpack');

module.exports = {
    entry: './src/index.js',

    output: {
        path: __dirname + '/public',
        filename: 'bundle.js'
    },

    devServer: {
        historyApiFallback: true,
        inline: true,
        port: 80,
        contentBase: __dirname + '/public'
    },

    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel',
                exclude: /node_modules/,
                query: {
                    cacheDirectory: true,
                    presets: ['env', 'stage-0', 'react']
                }
            }, {
                test: /\.css$/,
                loaders: ['style-loader', 'css-loader'],
                options: {
                    modules: true
                }
            }
        ]
    }
};
